# RiF2

## Introduction

RiF2 is a software that downloads all images from the main page of all websites it encounters. It is a better rewritten version of [RiF](https://gitlab.com/Isterix/rif).

## Main differences with RiF

### Code-wise

* It is much more readable as all functions are independant from each other
* For the same efficiency if not better, it has less lines of code

### Image-wise

* RiF2 doesn't download multiple times the same image
* It uses more top-level domains
* Images are named according to their original one
* Folders are named according to the website's name

### Console-wise

* Each website and image now only take one line in the console (instead of three)

## How to use it

You need [Ruby](https://www.ruby-lang.org) installed, along side [HTTParty](https://github.com/jnunemaker/httparty) and [Nokogiri](https://nokogiri.org/). That's it!

## License

This project is licensed under the GNU General Public License - see the LICENSE file for details