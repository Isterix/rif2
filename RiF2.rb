require 'httparty'
require 'nokogiri'
require "open-uri"

url_domains = [".com", ".net", ".fr", ".org", ".edu", ".gov", ".it", ".ch", ".uk", ".xyz", ".us", ".jp", ".cn", ".vip"]

def main(url_domains)
	url = generate_url(url_domains)
	valid = connect_url(url)
	if valid
		puts(" (SUCCESS)\n\n----- DEALING WITH " + url[0] + url[1] + url[2] + " -----")
		content = verify_url(valid)
		if content
			folder = check_folder(url)
			if folder
				image_folder = Dir.mkdir(folder)
				puts("The folder has been created successfully!") if File.exists?(folder)
				Dir.chdir(folder)
				puts("ERROR: The program did not move into the folder. It will not proceed to download the images.") if File.basename(Dir.getwd) != folder
				done = download_image(url, valid, content) if File.basename(Dir.getwd) == folder
				puts("RiF2 has finished dealing with this website!") if done
				puts("error") if !done
			end
		else
			puts("Looks like there was no image on this website!")
		end
		puts("----------\n\n")
	else
		puts(" (FAILED)")
	end
end

def generate_url(url_domains)
	url = ["https://www."]
	url << rand(36**rand(3..8)).to_s(36)
	url << url_domains[rand(0..url_domains.length - 1)]
	url
end

def connect_url(url)
	begin
		print("TRYING: " + url[0] + url[1] + url[2])
		response = HTTParty.get(url[0] + url[1] + url[2])
	rescue
		response = false
	ensure
		return response
	end
end

def verify_url(valid)
	parsed = Nokogiri::HTML(valid.body)
	images = parsed.css("img")
	if images.to_s.include? "e"
		return images
	else
		return false
	end
end

def check_folder(url)
	Dir.chdir("../") if File.basename(Dir.getwd) != "RiF2"
	folder_name = url[1] + " " + url[2][1..]
	unless File.exists?(folder_name)
		return folder_name
	else
		puts("RiF2 already dealt with this website!")
		return false
	end
end

def download_image(url, page, images)
	for image in images
		image = image['src'].to_s if image['src']
		image = image['data-src'].to_s if image['data-src'] #Some websites also use data-src, such as bul.ch
		if image['srcset'] #And some websites use srcset, such as tvr.org
			image = image['srcset'].to_s
			image = image[0..image.index(',') - 1]
			puts("Due to some images being in srcset, most of them will be ignored, sorry for the inconvenience!")
		end
		url_domains = [".com", ".net", ".fr", ".org", ".edu", ".gov", ".it", ".ch", ".uk", ".xyz", ".us", ".jp", ".cn"] #BAD CODE
		good_url = false
		for domain in url_domains
			good_url = true if image.include?(domain)
			if domain == ".jp" and image.include? ".jpg" #.jpg images can be mistaken as having an url starting with .jp
				good_url = false
				if image.count(".jp") > 1
					if image.include? "http://" or image.include? "https://"
						good_url = true
					end
				end
			end
		end
		if !good_url ##CREATE URL
			if page.body.to_s.include? "<base href="
				base = Nokogiri::HTML(page.body)
				base = base.css("base").to_s
				base = base[base.index('"') + 1..base.index('>') - 2]
				full_url = url[0] + url[1] + url[2]
				image = base + image if !base.include? full_url ##On some bad websites, base is also the website's url, yeah that's stupid
			end
			image = if image[0] != "/" then url[0] + url[1] + url[2] + "/" + image else url[0] + url[1] + url[2] + image end #ADD / AFTER URL OR NOT
		end
		image.slice!(0) while image[0] == "/"
		unless image[0..6] == "http://" or image[0..7] == "https://" #Add https
			image.insert(0, "https://")
		end
		print("DOWNLOADING: " + image)
		if image[-6..].include?(".") ##MODIFY URL
			filename = image[image.rindex("/") + 1..image.rindex(".") - 1]
			image << "?"
			filename << image[image.rindex(".")..image.index("?") - 1]
			image[-1] = ''
		else
			image << ".png"
			filename = image[-10..]
		end
		filename = filename.gsub("/", "") if filename.include? "/" #/ makes the system think it has to move directory; Remove all of them to prevent fileopen failing
		unless image.include? "base64"
			begin ##DOWNLOAD URL
				if !File.file?(filename)
					open(image) {|f|
						File.open(filename, "wb") do |file|
							file.puts f.read
						end
					}
					puts(" (SUCCESSFULLY DOWNLOADED)") if File.file?(filename)
					puts(" (DID NOT DOWNLOAD)") if !File.file?(filename)
				else
					puts(" (ALREADY DOWNLOADED)")
				end
			rescue => error
				puts(" (FAILED TO DOWNLOAD) (" + error.to_s + ")")
			end
		else
			puts(" (IGNORED; BASE64)")
		end
	end
	true
end

while true do
	main(url_domains)
end
